import React, { useEffect, useState } from 'react'
import { Card, Typography } from "@material-tailwind/react";
import { useDispatch, useSelector } from 'react-redux';
import { Decrement, Increment, cleanCart, delProduct } from '../systemRedux/AddToCartSlice';
import { Button } from "@material-tailwind/react";
import { MdDelete } from 'react-icons/md';
import { useNavigate } from 'react-router-dom';
import shoppingCart from "../assets/shopping_cart.png"
const CartPage = () => {

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { addToCartData, loading } = useSelector((state) => state.AddToCartState)

  console.log(addToCartData);

  const TABLE_HEAD = ["S.N", "Product", "Unit Price", "Quantity", "Total Price", "Actions"];



  return (
    <section className={`container mx-auto pt-10 ${addToCartData && addToCartData.length <= 3 && "h-[90vh] sm:h-[80vh] md:h-[75vh] lg:h-[70vh]"}`}>
      {addToCartData && addToCartData.length > 0 ? (
        <>
          <div className=' block md:hidden'>
            <div className='flex justify-center mb-3 text-green-800 tracking-wider font-mono font-bold text-lg'>
              <p className='border-x-2 border-green-600 px-3 w-fit'>The Table is Overflow</p>
            </div>
          </div>
          <div className='flex flex-col gap-5'>
            <Card className="w-full h-full overflow-auto">
              <table className="w-full min-w-max table-auto text-left">
                <thead>
                  <tr className='text-center'>
                    {TABLE_HEAD.map((head) => (
                      <th key={head} className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
                        <Typography
                          color="blue-gray"
                          className="font-semibold leading-none opacity-70"
                        >
                          {head}
                        </Typography>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>

                  {addToCartData && addToCartData.map((product, index) => (


                    <tr key={product.id} className='text-center'>
                      <td className="p-4 border-b border-blue-gray-50">
                        <Typography variant="small" color="blue-gray" className="font-normal">
                          {++index}
                        </Typography>
                      </td>
                      <td className="p-4 border-b border-blue-gray-50">
                        <Typography variant="small" color="blue-gray" className="font-normal">
                          {product.title}
                        </Typography>
                      </td>
                      <td className="p-4 border-b border-blue-gray-50">
                        <Typography variant="small" color="blue-gray" className="font-normal">
                          EGP {(product.price - (product.price * (product.discountPercentage / 100))).toFixed(2)}
                        </Typography>
                      </td>
                      <td className="flex justify-center p-4 border-b border-blue-gray-50">
                        <tr className={`${product.count !== 1 ? "cursor-pointer border-solid border-2 border-r-0 rounded rounded-r-none border-gray-200" : "cursor-pointer border-solid border-2 border-r-0 rounded rounded-r-none border-gray-200 bg-gray-200"}`}
                        >
                          <th>
                            <button
                              className="font-normal opacity-90 px-4 py-1 hover:text-blue-700"
                              disabled={product.count === 1}
                              onClick={() => dispatch(Decrement(product))}
                            >
                              -
                            </button>
                          </th>
                        </tr>
                        <tr className='border-solid border-2 border-gray-200'>
                          <th className='px-4 py-1'>
                            <Typography
                              className="font-normal opacity-90"
                            >
                              {product.count}
                            </Typography>
                          </th>
                        </tr>
                        <tr className='cursor-pointer border-solid border-2 border-l-0  rounded rounded-l-none border-gray-200'>
                          <th>
                            <button
                              className="font-normal opacity-90 px-4 py-1 hover:text-blue-700"
                              onClick={() => dispatch(Increment(product))}
                            >
                              +
                            </button>
                          </th>
                        </tr>
                      </td>

                      <td className="p-4 border-b border-blue-gray-50">
                        <Typography variant="small" color="blue-gray" className="font-normal">
                          EGP {(product.price * product.count - product.price * product.discountPercentage / 100 * product.count).toFixed(2)}
                        </Typography>
                      </td>

                      <td className="p-4 border-b border-blue-gray-50">
                        <Typography variant="small"
                          className="cursor-pointer text-green-600 font-bold hover:text-green-400 transition-colors duration-200"
                          onClick={() => dispatch(delProduct(product))}
                        >
                          Delete
                        </Typography>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </Card>

            <div className='flex flex-col gap-5 sm:gap-1 bg-white rounded-b-xl p-5'>
              <div className='flex flex-col sm:flex-row items-center sm:items-start gap-5 sm:gap-0 justify-between'>
                <Button onClick={() => dispatch(cleanCart())} className='flex gap-2 text-green-900 font-bold text-sm items-center border-green-900' variant="outlined"><MdDelete /> CLEAR CART</Button>
                <p>
                  Total {`(${addToCartData && addToCartData.length})`} Items : <span className='text-green-800 text-xl font-bold'>EGP {addToCartData && addToCartData.map((products) => (products.price * products.count - products.price * products.discountPercentage / 100 * products.count).toFixed(2)).reduce((x, o) => { return x + o })}</span>
                </p>

              </div>
              <div className='flex justify-center sm:justify-end'>
                <Button className='bg-green-900 hover:shadow-green-700'>Check Out</Button>

              </div>
            </div>

          </div>
        </>
      ) : (
        <div className='h-[70vh]'>
          <div className='flex gap-5 h-full flex-col justify-center items-center'>
            <img className='w-[150px] max-w-full' src={`${shoppingCart}`} alt="Shopping Cart" />
            <p className='font-normal leading-none opacity-70 tracking-wide'>Your shopping cart is empty</p>
            <Button className='bg-green-900 hover:shadow-green-700 tracking-wide' onClick={() => navigate("/")}>Go shopping Now</Button>

          </div>
        </div>
      )}
    </section>
  )
}

export default CartPage