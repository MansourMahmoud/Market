import React, { useEffect } from 'react'
import { Alert } from "@material-tailwind/react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Button,
} from "@material-tailwind/react";
import { GetAllProducts } from '../../systemRedux/GetAllProductsSlice';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllProductsCategories } from '../../systemRedux/GetAllProductsCategoriesSlice';
import { useNavigate } from 'react-router-dom';



const SectionProducts = () => {

  const navigate = useNavigate()
  const { allProducts, data } = useSelector((state) => state.GetAllProductsState)
  const { productsCategories } = useSelector((state) => state.GetAllProductsCategoriesState)
  const dispatch = useDispatch()



  useEffect(() => {
    dispatch(GetAllProducts())
    dispatch(GetAllProductsCategories())
  }, []);


  const deinamicCategorie = productsCategories?.filter((categorie, index) => index <= 5)

  return (
    <div className='container mx-auto px-5 sm:px-2'>

      <div className='shadow-lg mt-[50px]'>
        <Alert
          className="rounded-none text-sm md:text-md lg:text-xl border-l-4 border-[#2ec946] bg-[#fff]/10 font-semibold text-gray-500"
        >
          SEE OUR PRODUCTS
        </Alert>
      </div>

      <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-6 justify-center items-center'>
        {
          allProducts && allProducts.map((product) => (
            <div className='mt-[50px] flex justify-center'>
              <Card className="w-80 sm:w-full rounded-md shadow-xl">
                <CardHeader floated={false} className=" w-full m-0 flex items-center rounded-none">
                  <img className='h-72 lg:h-80  w-full object-cover object-center' src={`${product.images && product.images[0]}`} alt="profile-picture" />
                </CardHeader>
                <CardBody className='flex flex-col gap-5 items-center justify-center'>
                  <Typography color="blue-gray" className="mb-2 w-full text-center font-semibold relative after:absolute after:content-[' '] after:bottom-[-10px] after:left-0 after:w-full after:h-[0.1px] after:bg-gray-300">
                    Brand: {product.brand}
                  </Typography>
                  <Typography className="text-center">
                    {product.title}
                  </Typography>
                  <Typography className='flex justify-center items-center gap-2 flex-wrap'>
                    <span className='line-through text-sm '>EGP {product.price}</span>
                    <span className='font-bold text-md relative after:absolute after:content-[" "] after:bottom-[-7px] after:left-0 after:w-16 after:h-[1px] after:bg-green-500'>EGP {(product.price - (product.price * (product.discountPercentage / 100))).toFixed(2)}</span>
                    <span className='text-green-700 text-sm'>(% Off)</span>
                  </Typography>
                </CardBody>
                <CardFooter className="pt-0 ">
                  <Button
                    ripple={false}
                    fullWidth={true}
                    className="bg-blue-gray-900/10 text-blue-gray-900 shadow-none hover:scale-105 hover:shadow-none focus:scale-105 focus:shadow-none active:scale-100"
                    onClick={()=> navigate(`/product/${product.id}`)}
                  >
                    Show Details
                  </Button>
                </CardFooter>
              </Card>
            </div>
          ))
        }
      </div>

      {deinamicCategorie && deinamicCategorie.map((categorie) => (
        <>
          <div className='shadow-lg mt-[50px]'>
            <Alert
              className="rounded-none text-sm md:text-md lg:text-xl border-l-4 border-[#2ec946] bg-[#fff]/10 font-semibold text-gray-500"
            >
              SEE OUR {categorie.toUpperCase()}
            </Alert>
          </div>

          <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-6 justify-center items-center'>
            {
              allProducts && allProducts.filter((product) => product.category === categorie)
                .map((product) => (
                  <div className='mt-[50px] flex justify-center'>
                    <Card className="w-80 sm:w-full rounded-md shadow-xl">
                      <CardHeader floated={false} className=" w-full m-0 flex items-center rounded-none">
                        <img className='h-72 lg:h-80  w-full object-cover object-center' src={`${product.images && product.images[0]}`} alt="profile-picture" />
                      </CardHeader>
                      <CardBody className='flex flex-col gap-5 items-center justify-center'>
                        <Typography color="blue-gray" className="mb-2 w-full text-center font-semibold relative after:absolute after:content-[' '] after:bottom-[-10px] after:left-0 after:w-full after:h-[0.1px] after:bg-gray-300">
                          Brand: {product.brand}
                        </Typography>
                        <Typography className="text-center">
                          {product.title}
                        </Typography>
                        <Typography className='flex justify-center items-center gap-2 flex-wrap'>
                          <span className='line-through text-sm '>EGP {product.price}</span>
                          <span className='font-bold text-md relative after:absolute after:content-[" "] after:bottom-[-7px] after:left-0 after:w-16 after:h-[1px] after:bg-green-500'>EGP {(product.price - (product.price * (product.discountPercentage / 100))).toFixed(2)}</span>
                          <span className='text-green-700 text-sm'>(% Off)</span>
                        </Typography>
                      </CardBody>
                      <CardFooter className="pt-0 ">
                        <Button
                          ripple={false}
                          fullWidth={true}
                          className="bg-blue-gray-900/10 text-blue-gray-900 shadow-none hover:scale-105 hover:shadow-none focus:scale-105 focus:shadow-none active:scale-100"
                          onClick={()=> navigate(`/product/${product.id}`)}
                        >
                          Add to Cart
                        </Button>
                      </CardFooter>
                    </Card>
                  </div>
                ))
            }
          </div>
        </>



      ))}
    </div>
  )
}

export default SectionProducts