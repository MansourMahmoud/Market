import slider1 from '../../assets/slider_img_1.jpg'
import slider2 from '../../assets/slider_img_2.jpg'
import Slider from "react-slick";

const SectionCarousel = () => {


  const images = [
    { src: slider1 },
    { src: slider2 },
  ]

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 2000,
    nextArrow: <></>,
    prevArrow: <></>,
  };



  return (
    <section className='py-[15px] md:py-[35px] bg-white'>
      <div className='container mx-auto outline-8 outline-[10px_white]'>
        <Slider {...settings} className=''>

          {images.map((image) => (
            <div className='flex items-center justify-center w-full lg:h-[320px]' key={image.src}>
              <img className='object-contain' src={image.src} alt="img" />
            </div>
          ))}
        </Slider>
      </div>
    </section>

  )
}

export default SectionCarousel

