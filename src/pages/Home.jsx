import SectionCarousel from './home_pages/SectionCarousel'
import SectionProducts from './home_pages/SectionProducts'

const Home = () => {

  return (
    <div>
      <SectionCarousel/>
      <SectionProducts/>
    </div>
  )
}

export default Home