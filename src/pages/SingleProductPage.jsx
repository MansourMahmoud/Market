import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { GetASingleProduct, Next, Prov } from '../systemRedux/GetASingleProductSlice';
import { Button } from "@material-tailwind/react";
import {
    Lightbox,
    initTE,
} from "tw-elements";
import { Card, Typography } from "@material-tailwind/react";
import { BiCart } from 'react-icons/bi';
import { AddToCartFun, Decrement, Increment } from '../systemRedux/AddToCartSlice';




const SingleProductPage = () => {
    initTE({ Lightbox });

    const { productNumber } = useParams()
    const dispatch = useDispatch()

    const { dataProduct, loading } = useSelector((state) => state.GetASingleProductState)

    useEffect(() => {
        dispatch(GetASingleProduct(productNumber && productNumber))
    }, [productNumber]);


    return (
        <section className='container mx-auto p-5'>
            {loading ? (
                <div className='h-[70vh] flex items-center justify-center'>
                    <span className="loader"></span>

                </div>
            ) : (
                <div className='grid grid-cols-1 md:grid-cols-2 gap-6 bg-white p-5'>
                    {/* الجزء الاول الخاص بالصور */}
                    <div className='flex flex-col'>
                        <div data-te-lightbox-init className="flex flex-col gap-4">
                            <div>
                                <img
                                    src={`${dataProduct && dataProduct.thumbnail && dataProduct.thumbnail}`}
                                    data-te-img={`${dataProduct && dataProduct.thumbnail && dataProduct.thumbnail}`}
                                    alt={`${dataProduct && dataProduct.title && dataProduct.title}`}
                                    className="h-[350px] lg:h-[450px] w-full cursor-zoom-in data-[te-lightbox-disabled]:cursor-auto" />
                            </div>
                            <div className='flex justify-center gap-5 w-full flex-wrap md:gap-5'>
                                {dataProduct && dataProduct.images && dataProduct.images.map((img, index) => (
                                    <div key={index} className={`${dataProduct && dataProduct.images && dataProduct.images.length === 1 ? "w-48" : "w-[120px] md:w-[80px] lg:w-[90px] max-w-full flex items-center justify-start "}`}>
                                        <img
                                            src={`${img}`}
                                            data-te-img={`${img}`}
                                            alt={`${dataProduct && dataProduct.title && dataProduct.title}`}
                                            className="h-[120px] w-full object-contain cursor-zoom-in data-[te-lightbox-disabled]:cursor-auto hover:border-solid hover:border-2 hover:p-4 hover:border-green-500 transition-all duration-200 ease-linear" />
                                    </div>
                                ))}
                            </div>
                        </div>

                    </div>
                    {/* االجزء الثاني الخاص بمعلومات المنتج*/}
                    <div className='flex flex-col gap-5 py-5'>
                        <p className='text-center md:text-start font-bold relative after:absolute after:content-[" "] after:bottom-[-10px] after:left-0 after:w-full after:h-[0.1px] after:bg-gray-300'>{dataProduct && dataProduct.title}</p>
                        <p className='mt-2 text-gray-500 text-center md:text-start'>{dataProduct && dataProduct.description}</p>
                        <div className='flex gap-5 flex-wrap justify-center md:justify-start text-gray-600'>
                            <p className='relative after:absolute after:content-[" "] after:bg-green-500 after:w-[2px] after:h-full after:top-0 after:bottom-0 after:right-[-10px]'>Rating: {dataProduct && dataProduct.rating && dataProduct.rating}</p>
                            <p className='relative after:absolute after:content-[" "] after:bg-green-500 after:w-[2px] after:h-full after:top-0 after:bottom-0 after:right-[-10px]'>Brand : {dataProduct && dataProduct.brand}</p>
                            <p>category : {dataProduct && dataProduct.category}</p>
                        </div>
                        <div className='flex flex-col gap-3 bg-gray-100 p-5'>
                            <div className='flex gap-3 flex-wrap justify-center md:justify-start text-sm text-gray-500'>
                                <p className='line-through'>EGP {dataProduct && dataProduct.price}</p>
                                <p>Inclusive of taxes</p>
                            </div>
                            <div className='flex gap-5 flex-wrap justify-center md:justify-start'>
                                <p className='text-xl text-green-700 font-bold'>EGP {dataProduct && (dataProduct.price - (dataProduct.price * (dataProduct.discountPercentage / 100))).toFixed(2)}</p>
                                <p className='bg-green-800 px-5 font-serif text-white text-sm items-center flex'>{dataProduct && dataProduct.discountPercentage}% Off</p>

                            </div>
                        </div>
                        <div className='flex items-center flex-wrap justify-center md:justify-start gap-3'>
                            <p className='text-gray-700'>Quantity:</p>
                            <div className="flex items-center gap-4">
                                <Card className="w-full h-full my-5">
                                    <table className="w-full min-w-max  ">
                                        <thead className='flex bg-white'>
                                            <tr className={`${dataProduct && dataProduct.count !== 1 ? "cursor-pointer border-solid border-2 border-r-0 rounded rounded-r-none border-gray-200" : "cursor-pointer border-solid border-2 border-r-0 rounded rounded-r-none border-gray-200 bg-gray-200"}`}
                                            >
                                                <th>
                                                    <button
                                                        className="font-normal opacity-90 px-4 py-1 hover:text-blue-700"
                                                        disabled={dataProduct && dataProduct.count === 1}
                                                        onClick={() => dispatch(Prov(dataProduct && dataProduct))}
                                                    >
                                                        -
                                                    </button>
                                                </th>
                                            </tr>
                                            <tr className='border-solid border-2 border-gray-200'>
                                                <th className='px-4 py-1'>
                                                    <Typography
                                                        className="font-normal opacity-90"
                                                    >
                                                        {dataProduct && dataProduct.count}
                                                    </Typography>
                                                </th>
                                            </tr>
                                            <tr className='cursor-pointer border-solid border-2 border-l-0  rounded rounded-l-none border-gray-200'>
                                                <th>
                                                    <button
                                                        className="font-normal opacity-90 px-4 py-1 hover:text-blue-700"
                                                        onClick={() => dispatch(Next(dataProduct && dataProduct))}
                                                    >
                                                        +
                                                    </button>
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </Card>
                            </div>
                        </div>
                        <div className="flex gap-4 flex-col sm:flex-row items-center justify-center md:justify-start normal-case ">
                            <Button
                                color="green"
                                className='flex gap-2 items-center justify-center normal-case'
                                onClick={() => dispatch(AddToCartFun(dataProduct && dataProduct))}
                            >
                                <span className='text-xl'><BiCart /></span>
                                <span className='text-sm'>Add to Cart</span></Button>
                            <Button color="green" className='text-sm normal-case '>Buy Now</Button>
                        </div>
                    </div>
                </div>
            )}

        </section>
    )
}

export default SingleProductPage