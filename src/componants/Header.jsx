import React, { useEffect, useRef, useState } from 'react'
import {
    Navbar,
    MobileNav,
    Typography,
    Button,
    IconButton,
    Input,
    Drawer,
    List,
    ListItem,
    Badge,

} from "@material-tailwind/react";

import { BiSolidShoppingBag } from 'react-icons/bi';
import { BsFacebook } from 'react-icons/bs';
import { BsInstagram } from 'react-icons/bs';
import { BsCart4 } from 'react-icons/bs';
import { MdOutlineContactSupport } from 'react-icons/md';
import { MdOutlineSegment } from 'react-icons/md';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { GetAllProductsCategories } from '../systemRedux/GetAllProductsCategoriesSlice';
import shoppingCart from "../assets/shopping_cart.png"


const Header = () => {

    const { productsCategories } = useSelector((state) => state.GetAllProductsCategoriesState)
    const { addToCartData } = useSelector((state) => state.AddToCartState)
    const dispatch = useDispatch()


    useEffect(() => {
        dispatch(GetAllProductsCategories())
    }, []);


    const navigate = useNavigate()

    const [openNav, setOpenNav] = React.useState(false);

    React.useEffect(() => {
        window.addEventListener(
            "resize",
            () => window.innerWidth >= 960 && setOpenNav(false)
        );
    }, []);


    const funNavigatePageProducts = (category) => {
        navigate(`/category/${category}`)
    }


    const [isDisabled, setIsDisabled] = useState(true);

    const form = useRef()

    const handleSearch = (event) => {
        event.preventDefault()
        const value = event.target.children[0].children[0].value
        if(value !== ""){
            navigate(`/search/${value.toLowerCase()}`)
            event.target.children[0].children[0].value = ""
            setOpenNav(false)
            setIsDisabled(true)
        }

    }

    const handleInputChange = (event) => {
        const inputValue = event.target.value;
        const isValueEmpty = !inputValue || inputValue.trim() === '';
        setIsDisabled(isValueEmpty);
    };



    //this is for the input search and the category 
    const navList = (
        <div className='flex flex-col gap-2 items-start'>
            <form onSubmit={(event)=>handleSearch(event)} ref={form} className="relative flex-wrap flex w-full gap-2">
                <Input
                    type="search"
                    label="Type here..."
                    className="pr-24 w-full"
                    color='white'
                    containerProps={{
                        className: "max-w-full text-white",
                    }}
                    onChange={(event)=>handleInputChange(event)} // أضيف هنا حدث التغيير
                />
                <Button size="sm" type="submit" className="!absolute right-1 top-1 rounded bg-green-700 hover:shadow-green-700"
                    disabled={isDisabled}
                >
                    Search
                </Button>
            </form>
            <ul className="mb-4 mt-2 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center lg:gap-6 flex-wrap">
                {productsCategories && productsCategories
                    .filter((categorieFilter, index) =>
                        index < 8
                    ).map((categorie, index) => (
                        <Typography
                            as="li"
                            variant="small"
                            color="white"
                            className="p-1 font-normal cursor-pointer"
                            key={index}
                            onClick={() => setOpenNav(false)}
                        >
                            <a onClick={() => funNavigatePageProducts(categorie)} className="flex items-center hover:text-light-green-500 transition-colors duration-200">
                                {categorie.charAt(0).toUpperCase() + categorie.slice(1)}
                            </a>
                        </Typography>
                    ))}
            </ul>
        </div>
    );

    //this is for SideBar
    const [open, setOpen] = React.useState(false);
    const openDrawer = () => setOpen(true);
    const closeDrawer = () => setOpen(false);


    const itemsToShow = 6;

    const searchListRef = useRef(null);
    const [isHovered, setIsHovered] = useState(false)

    useEffect(() => {
        if (searchListRef.current) {
            const list = searchListRef.current;
            const items = list.children;
            if (items.length > itemsToShow) {
                list.style.maxHeight = `${itemsToShow * items[1].offsetHeight}px`;
            }
        }
    }, [isHovered]);


    return (
        <div>
            {/* Navbar */}
            <Navbar className="sticky bg-green-900 top-0 z-10 h-max max-w-full rounded-none py-2 px-4 lg:px-8 lg:py-4">
                <div className="text-white  flex flex-col gap-5">
                    <div className='hidden lg:block relative'>
                        <div className='flex items-center justify-between gap-4 after:absolute after:content-[" "] after:bg-white after:w-full after:h-[1px] after:bottom-[-5px] after:left-0'>
                            <div className='flex items-center gap-3'>
                                <span>Follow us on</span>
                                {/* this is icons the Facebook and the Instagram in top Navbar left */}
                                <span><BsFacebook className='cursor-pointer hover:text-blue-500 transition-colors duration-200' /></span>
                                <span><BsInstagram className='cursor-pointer hover:text-[#bc2a8d] transition-colors duration-200' /></span>
                                {/* end */}
                            </div>
                            <div className='flex items-center gap-3'>

                                <span className='flex '>
                                    <span>
                                        <MdOutlineContactSupport className='text-xl' />
                                    </span>
                                    <span className='relative'>
                                        <span className='after:absolute after:content-[" "] after:bg-white after:w-[3px] after:h-full after:top-0 after:bottom-0 after:right-[-7px] hover:text-light-green-500 hover:border-b-2 hover:border-light-green-500 transition-colors duration-200 cursor-pointer'>Support</span>
                                    </span>
                                </span>
                                <span className='relative'>
                                    <span className='after:absolute after:content-[" "] after:bg-white after:w-[3px] after:h-full after:top-0 after:bottom-0 after:right-[-7px] hover:text-light-green-500 hover:border-b-2 hover:border-light-green-500 transition-colors duration-200 cursor-pointer'>
                                        Register
                                    </span>
                                </span>
                                <span className='hover:text-light-green-500 transition-colors duration-200 cursor-pointer'>
                                    Log in
                                </span>
                            </div>
                        </div>
                    </div>

                    <div className="flex items-center justify-between gap-4 relative">
                        <Typography
                            as="a"
                            className="mr-4 cursor-pointer py-1.5 font-medium flex items-center gap-1"
                        >
                            {/* This is icon to open Sidebar */}
                            <span onClick={openDrawer}>
                                <MdOutlineSegment className='hover:text-light-green-500 transition-colors duration-200 text-[25px] md:text-[30px] lg:text-[40px]' />
                            </span>
                            {/* end */}

                            {/* this is an icon shoping next to the name of the Website left */}
                            <span><BiSolidShoppingBag className='text-[25px] md:text-[30px] lg:text-[40px]' /></span>
                            {/* end */}

                            {/* The name of the Website */}
                            <span className='hover:text-light-green-500 transition-colors duration-200 text-[25px] md:text-[40px] lg:text-[30px]' onClick={() => navigate("/")}>Market</span>
                            {/* end */}

                        </Typography>

                        {/* this is for the input search and the category   */}
                        <div className="mr-4 grow hidden lg:block">{navList}</div>
                        {/* end */}

                        <div className=''>
                            <div
                                className="hidden lg:inline-block transition-all duration-300"
                                onMouseEnter={() => setIsHovered(true)}
                                onMouseLeave={() => setIsHovered(false)}
                            >

                                {/* This is Add To Cart */}
                                <Badge content={addToCartData && addToCartData.length} className={`bg-white text-black font-bold ${addToCartData && addToCartData.length === 0 ? "p-2" : "text-green-500"}`}>
                                    <BsCart4 onClick={() => navigate("/cart")} className='hover:text-light-green-500 text-[30px] cursor-pointer text-[#fff] shadow-xl transition-colors duration-200' />
                                </Badge>
                                {
                                    isHovered && (
                                        addToCartData && addToCartData.length > 0 ? (
                                            <ul className="absolute w-[30%] bg-white flex flex-col text-black overflow-auto right-0 border border-green-900 font-serif rounded-xl" ref={searchListRef}>
                                                <li className='sticky top-0 z-10 h-max max-w-full'>
                                                    <div className='text-center py-6 font-serif text-xl border-b-2 bg-green-50 border-green-400 text-green-900 font-bold shadow-md'>
                                                        <p>Recenlty Added Products</p>
                                                    </div>
                                                </li>
                                                {addToCartData.map((result, index) => (
                                                    <li key={index} className="flex flex-col gap-2 border-b-2 mb-2 p-2 shadow"
                                                        onClick={() => navigate(`/product/${result.id}`)}
                                                    >

                                                        <div className='flex gap-5 items-center hover:bg-gray-300 hover:cursor-pointer p-2 rounded-md'>
                                                            <div className='w-20'>
                                                                <img alt="product" className='rounded-full h-[60px] object-contain w-full' src={`${result.thumbnail}`} />
                                                            </div>
                                                            <div className='flex justify-between w-full pr-5'>
                                                                <p>{result.title}</p>
                                                                <p className='text-green-700 font-bold'>EGP {(result.price - (result.price * (result.discountPercentage / 100))).toFixed(2)}</p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                ))}
                                                <li>
                                                    <div className='py-4 flex justify-end pr-3'>
                                                        <Button className='bg-green-900 hover:shadow-green-700 tracking-wide' onClick={() => navigate("/cart")}>View My Shopping Cart</Button>

                                                    </div>
                                                </li>
                                            </ul>
                                        ) : (
                                            <ul className="absolute w-[30%] bg-white flex flex-col text-black overflow-auto right-0 border border-green-900 font-serif rounded-xl" ref={searchListRef}>
                                                <li className='sticky top-0 z-10 h-max max-w-full'>
                                                    <div className='text-center py-6 font-serif tracking-wide text-xl border-b-2 bg-green-50 border-green-400 text-green-900 font-bold shadow-md'>
                                                        <p>Recenlty Added Products</p>
                                                    </div>
                                                </li>
                                                <li className='h-96'>
                                                    <div className='flex flex-col h-full gap-3 justify-center items-center'>
                                                        <div className='w-28'>
                                                            <img alt="Shopping Cart" className='rounded-full shadow-xl' src={`${shoppingCart}`} />
                                                        </div>
                                                        <div className='text-black'>
                                                            <p className='font-sans font-medium tracking-wide'>
                                                                No Products Yet!
                                                            </p>
                                                        </div>
                                                        <div className=''>
                                                            <Button className='bg-green-900 hover:shadow-green-700 tracking-wide' onClick={() => navigate("/")}>Go Shopping Now!</Button>

                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        )
                                    )
                                }
                            </div>
                        </div>

                        {/* this is an icon to open Navbar in small screens */}
                        <IconButton
                            variant="text"
                            className="ml-auto h-6 w-6 text-inherit hover:bg-transparent focus:bg-transparent active:bg-transparent lg:hidden"
                            ripple={true}
                            onClick={() => setOpenNav(!openNav)}
                        >
                            {openNav ? (
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    className="h-6 w-6"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth={2}
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M6 18L18 6M6 6l12 12"
                                    />
                                </svg>
                            ) : (
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    className="h-6 w-6"
                                    fill="none"
                                    stroke="currentColor"
                                    strokeWidth={2}
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        d="M4 6h16M4 12h16M4 18h16"
                                    />
                                </svg>
                            )}
                        </IconButton>
                        {/* end */}
                    </div>

                </div>
                {/* NavBar in Mobile */}
                <MobileNav open={openNav}>
                    <div className='flex flex-col gap-5 flex-wrap max-w-full'


                    >
                        <div className='relative after:absolute after:content-[" "] after:bg-white after:w-full after:h-[1px] after:bottom-[-5px] after:left-0'>
                            <div className='flex items-center justify-between flex-wrap gap-3'>
                                <div className='flex flex-wrap items-center gap-3'>
                                    <span>Follow us on</span>
                                    <span onClick={() => setOpenNav(false)}><BsFacebook /></span>
                                    <span onClick={() => setOpenNav(false)}><BsInstagram /></span>
                                </div>
                                <div className='flex items-center gap-3'>

                                    <span className='flex '>
                                        <span>
                                            <MdOutlineContactSupport className='text-xl' />
                                        </span>
                                        <span className='relative'>
                                            <span className='after:absolute after:content-[" "] after:bg-white after:w-[3px] after:h-full after:top-0 after:bottom-0 after:right-[-7px] hover:text-light-green-500 hover:border-b-2 hover:border-light-green-500 transition-colors duration-200 cursor-pointer'>Support</span>
                                        </span>

                                    </span>

                                    <span className='relative'>
                                        <span className='after:absolute after:content-[" "] after:bg-white after:w-[3px] after:h-full after:top-0 after:bottom-0 after:right-[-7px] hover:text-light-green-500 hover:border-b-2 hover:border-light-green-500 transition-colors duration-200 cursor-pointer'>
                                            Register
                                        </span>
                                    </span>

                                    <span className='hover:text-light-green-500 transition-colors duration-200'>
                                        Log in
                                    </span>

                                </div>
                            </div>
                        </div>

                        <div className='flex flex-col flex-wrap'>
                            <div>
                                {navList}
                            </div>
                            <div>
                                <div className='mb-3'>
                                    <Badge content={addToCartData && addToCartData.length} className={`bg-white text-black font-bold ${addToCartData && addToCartData.length === 0 ? "p-2" : "text-green-500"}`}>
                                        <BsCart4 onClick={() => {navigate("/cart"); setOpenNav(false)}} className='text-[30px] cursor-pointer text-[#fff] shadow-xl transition-colors duration-200' />
                                    </Badge>
                                </div>
                            </div>
                        </div>
                    </div>

                </MobileNav>
                {/* End MobileNav */}


            </Navbar>


            {/* this is for SideBar */}
            <React.Fragment>
                <Drawer className='overflow-auto' open={open} onClose={closeDrawer}>
                    <div className="mb-2 flex items-center justify-between p-4">
                        <Typography variant="h5" color="blue-gray">
                            Side Menu
                        </Typography>
                        <IconButton variant="text" color="blue-gray" onClick={closeDrawer}>
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={2}
                                stroke="currentColor"
                                className="h-5 w-5"
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M6 18L18 6M6 6l12 12"
                                />
                            </svg>
                        </IconButton>
                    </div>
                    <List onClick={closeDrawer}>

                        {productsCategories && productsCategories.map((categorie, index) => (
                            <ListItem key={index} onClick={() => funNavigatePageProducts(categorie)}>
                                {categorie.charAt(0).toUpperCase() + categorie.slice(1)}
                            </ListItem>
                        ))}
                    </List>
                </Drawer>
            </React.Fragment>
            {/* End SideBar */}
        </div>
    )
}

export default Header