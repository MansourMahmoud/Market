import Home from "./pages/Home"
import Header from "./componants/Header"
import Footer from "./componants/Footer"
import "../node_modules/slick-carousel/slick/slick.css"; 
import "../node_modules/slick-carousel/slick/slick-theme.css";
import { Routes, Route } from "react-router-dom";
import ProductsPage from "./pages/ProductsPage";
import SingleProductPage from "./pages/SingleProductPage";
import SearchPage from "./pages/SearchPage";
import CartPage from "./pages/CartPage";
function App() {

  return (
    <>
      <div>
        <Header/>

      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/category/:namecategory" element={<ProductsPage/>}/>
        <Route path="/product/:productNumber" element={<SingleProductPage/>}/>
        <Route path="/cart" element={<CartPage/>}/>
        <Route path="/search/:productName" element={<SearchPage/>}/>
      </Routes>

      
    <Footer/>

      </div>
    </>
  )
}

export default App
