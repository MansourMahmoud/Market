import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from 'axios'
export const GetAllProducts = createAsyncThunk("GetAllProducts", async(id, thunkAPI)=>{
    const {rejectWithValue} = thunkAPI;

    try{
        const response = await axios('https://dummyjson.com/products')
        const data = await response.data
        return data
    }catch(err){
        return rejectWithValue(err)
    }

})


export const GetAllProductsSlice =  createSlice({
    name: "GetAllProductsSlice",
    initialState: {
        loading: false,
        error : null,
        data : {},
        allProducts : [],
    },
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(GetAllProducts.pending, (state)=>{
            state.loading = true
        })
        builder.addCase(GetAllProducts.fulfilled, (state, action)=>{
            state.loading = false
            state.allProducts = action.payload.products
            state.data = action.payload      
        })
        builder.addCase(GetAllProducts.rejected, (state, action)=>{
            state.loading = false
            state.error = action.payload.message
        })
    },
})

export const GetAllProductsState = GetAllProductsSlice.reducer