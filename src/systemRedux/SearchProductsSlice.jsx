import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from 'axios'




export const SearchProducts = createAsyncThunk("SearchProducts", async(id, thunkAPI)=>{
    const {rejectWithValue} = thunkAPI;

    console.log(id);
    try{
        const response = await axios(`https://dummyjson.com/products/search?q=${id}`)
        const data = await response.data
        return data
    }catch(err){
        return rejectWithValue(err)
    }

})


export const SearchProductsSlice =  createSlice({
    name: "SearchProductsSlice",
    initialState: {
        loading: false,
        error : null,
        productSearchData : [],
        searchData : {},
    },
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(SearchProducts.pending, (state)=>{
            state.loading = true
        })

        builder.addCase(SearchProducts.fulfilled, (state, action)=>{
            state.loading = false
            state.searchData = action.payload.products
            state.productSearchData = action.payload
            
        })

        builder.addCase(SearchProducts.rejected, (state, action)=>{
            state.loading = false
            state.error = action.payload.message
        })
    },
})

export const SearchProductsState = SearchProductsSlice.reducer