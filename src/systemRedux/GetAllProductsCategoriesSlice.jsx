import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from 'axios'




export const GetAllProductsCategories = createAsyncThunk("GetAllProductsCategories", async(id, thunkAPI)=>{
    const {rejectWithValue} = thunkAPI;

    try{
        const response = await axios('https://dummyjson.com/products/categories')
        const data = await response.data
        return data
    }catch(err){
        return rejectWithValue(err)
    }

})


export const GetAllProductsCategoriesSlice =  createSlice({
    name: "GetAllProductsCategoriesSlice",
    initialState: {
        loading: false,
        error : null,
        productsCategories : [],
    },
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(GetAllProductsCategories.pending, (state)=>{
            state.loading = true
        })

        builder.addCase(GetAllProductsCategories.fulfilled, (state, action)=>{
            state.loading = false
            state.productsCategories = action.payload
            
        })

        builder.addCase(GetAllProductsCategories.rejected, (state, action)=>{
            state.loading = false
            state.error = action.payload.message
        })
    },
})

export const GetAllProductsCategoriesState = GetAllProductsCategoriesSlice.reducer