import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from 'axios'



export const SearchWithCategory = createAsyncThunk("SearchWithCategory", async(id, thunkAPI)=>{
    const {rejectWithValue} = thunkAPI;

    try{
        const response = await axios(`https://dummyjson.com/products/category/${id}`)
        const data = await response.data
        return data
    }catch(err){
        return rejectWithValue(err)
    }

})


export const SearchWithCategorySlice =  createSlice({
    name: "SearchWithCategorySlice",
    initialState: {
        loading: false,
        error : null,
        productCategorySearchData : {},
        searchCategoryData : [],
    },
    reducers:{},
    extraReducers:(builder)=>{
        builder.addCase(SearchWithCategory.pending, (state)=>{
            state.loading = true
        })

        builder.addCase(SearchWithCategory.fulfilled, (state, action)=>{
            state.loading = false
            state.searchCategoryData = action.payload.products
            state.productCategorySearchData = action.payload
        })

        builder.addCase(SearchWithCategory.rejected, (state, action)=>{
            state.loading = false
            state.error = action.payload.message
        })
    },
})

export const SearchWithCategoryState = SearchWithCategorySlice.reducer