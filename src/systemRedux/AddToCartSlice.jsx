import { createSlice } from "@reduxjs/toolkit";



export const AddToCartSlice = createSlice({
    name: "AddToCartSlice",
    initialState: {
        loading: false,
        error: null,
        addToCartData: [],
    },
    reducers: {
        AddToCartFun: (state, action) => {

            const existingProduct = state.addToCartData.find(
                (product) => product.id === action.payload.id
            );

            if (existingProduct) {
                const updatedCart = state.addToCartData.map((product) =>
                    product.id === existingProduct.id
                        ? { ...product, count: action.payload.count + product.count}
                        : product
                );
                state.addToCartData = updatedCart;
            } else {
                state.addToCartData = [
                    ...state.addToCartData,
                    { ...action.payload},
                ];
            }
        },

        Increment: (state, action) => {
            console.log(action.payload);
            let result = state.addToCartData.find(
                (data) => data.id === action.payload.id
              );
              if (result.count > 0) {
                result.count += 1;
                state.addToCartData = [...state.addToCartData];
              }
         

        },

        Decrement: (state, action) => {
            let result = state.addToCartData.find(
                (data) => data.id === action.payload.id
              );
              if (result.count > 1) {
                result.count -= 1;
                state.addToCartData = [...state.addToCartData];
              }
    },

    delProduct: (state, action) => {
        const productId = action.payload.id;
        const updatedAddToCartData = state.addToCartData.filter((item) => 
        item.id !== productId);
  
        state.addToCartData = updatedAddToCartData;
      },

      cleanCart : (state)=>{
        state.addToCartData = []
    },

    },


          
})

export const { AddToCartFun, Increment, Decrement, delProduct, cleanCart} = AddToCartSlice.actions
export const AddToCartState = AddToCartSlice.reducer