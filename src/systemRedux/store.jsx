import {configureStore} from '@reduxjs/toolkit'

import {GetAllProductsState} from './GetAllProductsSlice'
import { GetASingleProductState } from './GetASingleProductSlice' 
import { SearchProductsState } from './SearchProductsSlice'
import { GetAllProductsCategoriesState } from './GetAllProductsCategoriesSlice' 
import { SearchWithCategoryState } from './SearchWithCategorySlice'  
import { AddToCartState } from './AddToCartSlice' 

export const store = configureStore({
    reducer:{
        GetAllProductsState,
        GetASingleProductState,
        SearchProductsState,
        GetAllProductsCategoriesState,
        SearchWithCategoryState,

        // Add To Cart
        AddToCartState,
    },
})