import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from 'axios'




export const GetASingleProduct = createAsyncThunk("GetASingleProduct", async(id, thunkAPI)=>{
    const {rejectWithValue} = thunkAPI;

    try{
        const response = await axios(`https://dummyjson.com/products/${id}`)
        const data = await response.data
        return data
    }catch(err){
        return rejectWithValue(err)
    }

})


export const GetASingleProductSlice =  createSlice({
    name: "GetASingleProductSlice",
    initialState: {
        loading: false,
        error : null,
        dataProduct : {},
    },
    reducers:{
        Next : (state, action)=>{
            console.log(action.payload);
              if (action.payload.count > 0) {
                state.dataProduct = {...action.payload, count: action.payload.count + 1 };
              }
        },
        Prov : (state, action)=>{
            if (action.payload.count > 1) {
                state.dataProduct = {...action.payload, count: action.payload.count - 1 };
              }
        },
    },
    extraReducers:(builder)=>{
        builder.addCase(GetASingleProduct.pending, (state)=>{
            state.loading = true
        })
        builder.addCase(GetASingleProduct.fulfilled, (state, action)=>{
            state.loading = false
            state.dataProduct = { ...action.payload, count: 1 }  
        })
        builder.addCase(GetASingleProduct.rejected, (state, action)=>{
            state.loading = false
            state.error = action.payload.message
        })
    },
})

export const {Next, Prov} = GetASingleProductSlice.actions
export const GetASingleProductState = GetASingleProductSlice.reducer